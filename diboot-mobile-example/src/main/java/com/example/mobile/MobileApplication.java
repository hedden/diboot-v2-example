package com.example.mobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * mobile启动类
 *
 * @author : uu
 * @version : 2.0
 * @Date 2021/8/30  17:38
 */
@SpringBootApplication
public class MobileApplication {
    public static void main(String[] args) {
        SpringApplication.run(MobileApplication.class);
    }
}
