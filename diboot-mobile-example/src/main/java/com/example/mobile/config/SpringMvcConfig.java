/*
 * Copyright (c) 2015-2020, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.example.mobile.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import com.diboot.iam.service.IamAccountService;
import com.diboot.mobile.service.IamMemberService;
import com.diboot.mobile.service.WxMaAuthService;
import com.diboot.mobile.service.WxMpAuthService;
import com.diboot.mobile.service.impl.WxMaUserAuthServiceImpl;
import com.diboot.mobile.service.impl.WxMpUserAuthServiceImpl;
import com.diboot.mobile.starter.MobileProperties;
import me.chanjar.weixin.mp.api.WxMpService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Spring MVC配置
 *
 * @author www.dibo.ltd
 * @version 2.0
 * @date 2021/09/29
 */
@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.example.mobile"})
@MapperScan(basePackages = {"com.example.mobile.mapper"})
public class SpringMvcConfig {

    @Autowired
    private MobileProperties mobileProperties;

    /**
     * 微信小程序用户操作配置
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public WxMaAuthService wxMaAuthService(WxMaService wxMaService, IamMemberService iamMemberService, IamAccountService iamAccountService) {
        return new WxMaUserAuthServiceImpl(wxMaService, iamMemberService, iamAccountService);
    }

    /**
     * 微信公众号用户操作配置
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public WxMpAuthService wxMpAuthService(WxMpService wxMpService, IamMemberService iamMemberService, IamAccountService iamAccountService) {
        return new WxMpUserAuthServiceImpl(wxMpService, iamAccountService, iamMemberService, mobileProperties.getWxMp().getState());
    }

}