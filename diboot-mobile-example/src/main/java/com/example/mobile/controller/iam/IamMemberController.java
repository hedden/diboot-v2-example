package com.example.mobile.controller.iam;

import com.diboot.core.controller.BaseCrudRestController;
import com.diboot.core.vo.JsonResult;
import com.diboot.iam.annotation.Log;
import com.diboot.iam.annotation.Operation;
import com.diboot.mobile.entity.IamMember;
import com.diboot.mobile.vo.IamMemberVO;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 移动端用户controller
 *
 * @author : uu
 * @version : v1.0
 * @Date 2021/8/31  17:41
 */
@RestController
@RequestMapping
public class IamMemberController extends BaseCrudRestController<IamMember> {

    /***
     * 获取用户信息
     * @param id ID
     * @return
     * @throws Exception
     */
    @GetMapping("/{id}")
    public JsonResult getViewObjectMapping(@PathVariable("id") Long id) throws Exception {
        return super.getViewObject(id, IamMemberVO.class);
    }

    /***
     * 更新用户信息
     * @param entity
     * @return JsonResult
     * @throws Exception
     */
    @Log(operation = Operation.LABEL_UPDATE)
    @PutMapping("/{id}")
    public JsonResult updateEntityMapping(@PathVariable("id") Long id, @Valid @RequestBody IamMember entity) throws Exception {
        return super.updateEntity(id, entity);
    }
}
