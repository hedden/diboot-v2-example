package com.example.mobile.controller.iam;

import com.diboot.core.vo.JsonResult;
import com.diboot.iam.auth.AuthServiceFactory;
import com.diboot.iam.config.Cons;
import com.diboot.iam.dto.PwdCredential;
import com.diboot.iam.entity.IamUser;
import com.diboot.iam.util.IamSecurityUtils;
import com.diboot.mobile.entity.IamMember;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 退出登陆相关controller
 *
 * @author : uu
 * @version : v1.0
 * @Date 2021/8/31  08:58
 */
@Slf4j
@RestController
@RequestMapping("/h5")
public class H5AuthTokenController {

    /**
     * 用户登录获取token
     *
     * @param credential
     * @return
     * @throws Exception
     */
    @PostMapping("/auth/login")
    public JsonResult login(@RequestBody PwdCredential credential) throws Exception {
        credential.setAuthType(Cons.DICTCODE_AUTH_TYPE.PWD.name()).setUserTypeClass(IamUser.class);
        String authtoken = AuthServiceFactory.getAuthService(Cons.DICTCODE_AUTH_TYPE.PWD.name()).applyToken(credential);
        return JsonResult.OK(authtoken);
    }

    /**
     * 注销/退出
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/logout")
    public JsonResult logout() throws Exception {
        IamSecurityUtils.logout();
        return JsonResult.OK();
    }

    /**
     * 获取用户角色权限信息
     *
     * @return
     */
    @GetMapping("/userInfo")
    public JsonResult getUserInfo() {
        // 获取当前登录用户对象
        Object currentUser = IamSecurityUtils.getCurrentUser();
        return JsonResult.OK(currentUser);
    }
}
