package com.example.mobile.controller.iam;

import com.diboot.core.vo.JsonResult;
import com.diboot.mobile.service.WxMpAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信公众号登陆/退出
 *
 * @author : uu
 * @version : v1.0
 * @Date 2021/8/31  10:34
 */
@RestController
@Slf4j
@RequestMapping("/wx-mp")
public class WxMpAuthTokenController {

    @Autowired(required = false)
    private WxMpAuthService wxMpAuthService;

    /**
     * 公众号获取认证地址：主要目的获取前端微信重定向的认证地址
     *
     * @param url 前端提交的URL，用于前端重定向
     * @return
     * @throws Exception
     */
    @GetMapping("/auth/buildOAuthUrl")
    public JsonResult buildOAuthUrl4mp(@RequestParam(value = "url") String url) throws Exception {
        return JsonResult.OK(wxMpAuthService.buildOAuthUrl4mp(url));
    }

    /**
     * 通过code登陆，并获取用户信息
     *
     * @param code
     * @return
     * @throws Exception
     */
    @GetMapping("/auth/apply")
    public JsonResult apply(@RequestParam(value = "code") String code, @RequestParam(value = "state") String state) throws Exception {
        return JsonResult.OK(wxMpAuthService.applyToken(code, state));
    }

    /**
     * <pre>
     * 登陆后绑定授权
     * </pre>
     */
    @GetMapping("/bindMp")
    public JsonResult bindMp(@RequestParam(value = "code") String code, @RequestParam(value = "state") String state) throws Exception {
        return JsonResult.OK(wxMpAuthService.bindWxMp(code, state));
    }
}
