package com.example.mobile.controller.iam;

import com.diboot.core.vo.JsonResult;
import com.diboot.iam.auth.AuthServiceFactory;
import com.diboot.iam.config.Cons;
import com.diboot.iam.entity.IamUser;
import com.diboot.mobile.dto.MobileCredential;
import com.diboot.mobile.dto.WxMemberDTO;
import com.diboot.mobile.entity.IamMember;
import com.diboot.mobile.service.WxMaAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 微信小程序登陆/退出
 *
 * @author : uu
 * @version : v1.0
 * @Date 2021/8/31  10:34
 */
@Slf4j
@RestController
@RequestMapping("/wx-ma")
public class WxMaAuthTokenController {

    @Autowired(required = false)
    private WxMaAuthService wxMaAuthService;

    /**
     * 用户登录获取token
     *
     * @param credential
     * @return
     */
    @PostMapping("/auth/login")
    public JsonResult<String> login(@RequestBody MobileCredential credential) throws Exception {
        credential.setUserTypeClass(IamUser.class);
        String authToken = AuthServiceFactory
                .getAuthService(Cons.DICTCODE_AUTH_TYPE.WX_MP.name())
                .applyToken(credential);
        return JsonResult.OK(authToken);
    }

    /**
     * 微信小程序获取SessionInfo:主要目的获取openid
     *
     * @param code code
     * @return
     */
    @GetMapping("/auth/getSessionInfo")
    public JsonResult getSessionInfo(String code) throws Exception {
        return JsonResult.OK(wxMaAuthService.getSessionInfo(code));
    }

    /**
     * <pre>
     * 保存用户，并创建账号信息
     * </pre>
     */
    @PostMapping("/auth/getAndSaveWxMember")
    public JsonResult getAndSaveWxMember(@RequestBody WxMemberDTO wxInfoDTO) throws Exception {
        return JsonResult.OK(wxMaAuthService.getAndSaveWxMember(wxInfoDTO));
    }

    /**
     * <pre>
     * 登陆后绑定授权
     * </pre>
     */
    @PostMapping("/bindMa")
    public JsonResult bindMa(@RequestBody WxMemberDTO wxInfoDTO) throws Exception {
        return JsonResult.OK(wxMaAuthService.bindWxMa(wxInfoDTO));
    }

}