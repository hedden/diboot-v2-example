package com.example.mobile.handler;

import com.diboot.core.handler.DefaultExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
* 通用异常处理类
* @author uu
* @version 2.0
* @date 2021-09-28 21:09
*/
@ControllerAdvice
@Slf4j
public class GeneralExceptionHandler extends DefaultExceptionHandler{

}