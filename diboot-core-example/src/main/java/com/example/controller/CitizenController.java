/*
 * Copyright (c) 2015-2021, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.example.controller;

import com.diboot.core.util.D;
import com.diboot.core.util.S;
import com.diboot.core.vo.JsonResult;
import com.diboot.core.vo.Pagination;
import com.example.dto.CitizenDTO;
import com.example.entity.Citizen;
import com.example.entity.CitizenHouse;
import com.example.entity.House;
import com.example.mapper.CitizenHouseMapper;
import com.example.service.CitizenService;
import com.example.service.HouseService;
import com.example.vo.CitizenDetailVO;
import com.example.vo.CitizenListVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
* 居民 相关Controller
* @author MyName
* @version 1.0
* @date 2021-06-25
* Copyright © MyCompany
*/
@RestController
@RequestMapping("/citizen")
@Slf4j
public class CitizenController extends BaseCustomCrudRestController<Citizen> {
    @Autowired
    private CitizenService citizenService;
    @Autowired
    private HouseService houseService;
    @Autowired
    private CitizenHouseMapper citizenHouseMapper;
    /***
    * 查询ViewObject的分页数据
    * <p>
    * url请求参数示例: /list?field=abc&pageIndex=1&orderBy=abc:DESC
    * </p>
    * @return
    * @throws Exception
    */
    @GetMapping("/list")
    public JsonResult getViewObjectListMapping(CitizenDTO queryDto, Pagination pagination) throws Exception{
        return super.getViewObjectList(queryDto, pagination, CitizenListVO.class);
    }

    /***
    * 根据资源id查询ViewObject
    * @param id ID
    * @return
    * @throws Exception
    */
    @GetMapping("/{id}")
    public JsonResult getViewObjectMapping(@PathVariable("id")Long id) throws Exception{
        return super.getViewObject(id, CitizenDetailVO.class);
    }

    /***
    * 创建资源对象
    * @param entity
    * @return JsonResult
    * @throws Exception
    */
    @PostMapping("/")
    public JsonResult createEntityMapping(@Valid @RequestBody Citizen entity) throws Exception {
        return super.createEntity(entity);
    }

    /***
    * 根据ID更新资源对象
    * @param entity
    * @return JsonResult
    * @throws Exception
    */
    @PutMapping("/{id}")
    public JsonResult updateEntityMapping(@PathVariable("id")Long id, @Valid @RequestBody Citizen entity) throws Exception {
        return super.updateEntity(id, entity);
    }

    /***
    * 根据id删除资源对象
    * @param id
    * @return
    * @throws Exception
    */
    @DeleteMapping("/{id}")
    public JsonResult deleteEntityMapping(@PathVariable("id")Long id) throws Exception {
        return super.deleteEntity(id);
    }

    @GetMapping("/initData")
    public JsonResult initData(){
        List<Citizen> citizens = new ArrayList<>();
        List<House> houses = new ArrayList<>();
        List<CitizenHouse> citizenHouses = new ArrayList<>();
        String[] realnameRandom = {"张", "赵", "孙", "李", "王", "欧阳", "陈", "宋", "徐"};
        String[] realnameRandom2 = {"强", "帅", "武", "斌", "文", "福", "志强", "建国", "立"};
        String[] genderRandom = {"F", "M"};
        for(int i=0; i<9000; i++){
            // 开始一个批次
            citizens.clear();
            houses.clear();
            citizenHouses.clear();

            for(int j=0; j<100; j++){
                Citizen citizen = new Citizen();
                String lastName = realnameRandom[j%9];
                citizen.setRealname(lastName + realnameRandom2[j%9] + j)
                        .setIdCard(S.newRandomNum(18))
                        .setBirthdate(D.convert2Date("1088-08-18"))
                        .setGender("F")
                        .setAddress(genderRandom[j%2])
                        .setMobilephone("13"+S.newRandomNum(9));
                citizens.add(citizen);

                House house = new House().setCity("上海").setArea("浦东新区").setCertNo(S.newRandomNum(20))
                        .setEstateName(lastName+"家花园"+j+"期")
                        .setAddress(lastName+"甲路"+j+"号大街"+j+"号")
                        .setBuildingNo(S.newRandomNum(2)).setDoorNo(S.newRandomNum(4));
                houses.add(house);
            }
            citizenService.createEntities(citizens);
            houseService.createEntities(houses);

            for(Citizen citizen : citizens){
                if(citizen.getId() % 5 == 0){
                    citizenHouseMapper.insert(new CitizenHouse().setCitizenId(citizen.getId()).setHouseId(houses.get(0).getId()));
                    citizenHouseMapper.insert(new CitizenHouse().setCitizenId(citizen.getId()).setHouseId(houses.get(2).getId()));
                    citizenHouseMapper.insert(new CitizenHouse().setCitizenId(citizen.getId()).setHouseId(houses.get(5).getId()));
                    citizenHouseMapper.insert(new CitizenHouse().setCitizenId(citizen.getId()).setHouseId(houses.get(7).getId()));
                    citizenHouseMapper.insert(new CitizenHouse().setCitizenId(citizen.getId()).setHouseId(houses.get(9).getId()));
                }
            }
            System.out.println("第"+i+"批数据创建完成！");
        }
        return JsonResult.OK();
    }

} 