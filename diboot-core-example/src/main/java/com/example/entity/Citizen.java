/*
 * Copyright (c) 2015-2021, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.example.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.diboot.core.util.D;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
* 居民 Entity定义
* @author MyName
* @version 1.0
* @date 2021-06-25
* Copyright © MyCompany
*/
@Getter @Setter @Accessors(chain = true)
public class Citizen extends BaseCustomEntity {
    private static final long serialVersionUID = 6775746107329636068L;

    /**
    * gender字段的关联字典
    */
    public static final String DICT_GENDER = "GENDER";

    /**
    * 姓名 
    */
    @NotNull(message = "姓名不能为空")
    @Length(max=100, message="姓名长度应小于100")
    @TableField()
    private String realname;

    /**
    * 性别 
    */
    @Length(max=100, message="性别长度应小于100")
    @TableField()
    private String gender;

    /**
    * 身份证号 
    */
    @NotNull(message = "身份证号不能为空")
    @Length(max=100, message="身份证号长度应小于100")
    @TableField()
    private String idCard;

    /**
    * 现住址 
    */
    @Length(max=100, message="现住址长度应小于100")
    @TableField()
    private String address;

    /**
    * 电话 
    */
    @Length(max=100, message="电话长度应小于100")
    @TableField()
    private String mobilephone;

    /**
    * 出生日期 
    */
    @JsonFormat(pattern = D.FORMAT_DATE_Y4MD)
    @TableField()
    private Date birthdate;

    /**
    * 创建人 
    */
    @TableField()
    private Long createBy;

    /**
    * 更新时间 
    */
    @JsonFormat(pattern = D.FORMAT_DATETIME_Y4MDHM)
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NOT_NULL)
    private Date updateTime;


} 
