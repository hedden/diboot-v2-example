/*
 * Copyright (c) 2015-2021, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.example.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.diboot.core.util.D;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
* 房产 Entity定义
* @author MyName
* @version 1.0
* @date 2021-06-25
* Copyright © MyCompany
*/
@Getter @Setter @Accessors(chain = true)
public class House extends BaseCustomEntity {
    private static final long serialVersionUID = -6142568623318906337L;

    /**
    * 产证号 
    */
    @NotNull(message = "产证号不能为空")
    @Length(max=100, message="产证号长度应小于100")
    @TableField()
    private String certNo;

    /**
    * 城市 
    */
    @Length(max=100, message="城市长度应小于100")
    @TableField()
    private String city;

    /**
    * 区县 
    */
    @Length(max=100, message="区县长度应小于100")
    @TableField()
    private String area;

    /**
    * 地址 
    */
    @Length(max=100, message="地址长度应小于100")
    @TableField()
    private String address;

    /**
    * 社区名 
    */
    @Length(max=100, message="社区名长度应小于100")
    @TableField()
    private String estateName;

    /**
    * 楼栋 
    */
    @Length(max=100, message="楼栋长度应小于100")
    @TableField()
    private String buildingNo;

    /**
    * 户号 
    */
    @Length(max=100, message="户号长度应小于100")
    @TableField()
    private String doorNo;

    /**
    * 创建人 
    */
    @TableField()
    private Long createBy;

    /**
    * 更新时间 
    */
    @JsonFormat(pattern = D.FORMAT_DATETIME_Y4MDHM)
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NOT_NULL)
    private Date updateTime;


} 
