/*
 * Copyright (c) 2015-2021, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.example.service.impl;

import com.diboot.core.service.impl.BaseServiceImpl;
import com.example.entity.Citizen;
import com.example.mapper.CitizenMapper;
import com.example.service.CitizenService;
import com.example.vo.CitizenDetailVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* 居民相关Service实现
* @author MyName
* @version 1.0
* @date 2021-06-25
 * Copyright © MyCompany
*/
@Service
@Slf4j
public class CitizenServiceImpl extends BaseServiceImpl<CitizenMapper, Citizen> implements CitizenService {

    @Autowired
    private CitizenMapper citizenMapper;

    @Override
    public List<CitizenDetailVO> getCitizenVOByMybatisXML(int begin) {
        return citizenMapper.getCitizenVOByMybatisXML(begin);
    }

}
